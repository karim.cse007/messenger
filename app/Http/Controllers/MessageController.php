<?php

namespace App\Http\Controllers;

use App\Events\MessageEvent;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function getMessage()
    {
        return Message::with('user')->get();
    }
    public function sentMessage(Request $request)
    {
        $message = new Message();
        $message->user_id =Auth::id();
        $message->message =$request->message;
        $message->save();
        broadcast(new MessageEvent($message->load('user')))->toOthers();
        return ['success'=>'success'];
    }
}
